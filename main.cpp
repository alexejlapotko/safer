#include <cstdlib>
#include <cstdio>
#include "test.h"

int main() {	
#ifdef NDEBUG 
	freopen("log.txt", "w", stdout);
#endif
	test();
	
#ifdef _DEBUG
	system("PAUSE");
#endif
	return 0;
}