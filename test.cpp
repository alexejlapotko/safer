#include <time.h>
#include <iostream>
#include <chrono>

#include "test.h"
#include "safer.h"

#define ENCRYPTION_BUFFER_SIZE (TEST_DATA_SIZE + BLOCK_LEN)

using namespace std;

unsigned char * getTestData();
unsigned char * getRandKey();
void fillRand(void * begin, void * end);
bool cmpArrays(void * aBegin, void * aEnd, void * bBegin, void * bEnd);
double getSpeed(const chrono::system_clock::time_point &start, const chrono::system_clock::time_point &finish, const int data_size);

void test() {
	cout << "Test safer: " << (double)TEST_DATA_SIZE / 1e3 << " kb" << endl;

	auto data = getTestData();
	
	unsigned char * encrypted = new unsigned char[ENCRYPTION_BUFFER_SIZE];
	unsigned char * decrypted = new unsigned char[ENCRYPTION_BUFFER_SIZE];
	size_t encryptedSize, decryptedSize;

	SaferPlus safer(getRandKey());

	auto encrypt_start_time = chrono::high_resolution_clock::now();
	safer.encrypt(data, TEST_DATA_SIZE, encrypted, &encryptedSize);
	auto encrypt_finish_time = chrono::high_resolution_clock::now();
	
	auto decrypt_start_time = chrono::high_resolution_clock::now();
	safer.decrypt(encrypted, encryptedSize, decrypted, &decryptedSize);
	auto decrypt_finish_time = chrono::high_resolution_clock::now();
	
	if (cmpArrays(data, data + TEST_DATA_SIZE, decrypted, decrypted + TEST_DATA_SIZE)) {
		cout << "OK" << endl;

		auto encrypt_speed = getSpeed(encrypt_start_time, encrypt_finish_time, TEST_DATA_SIZE);
		auto decrypt_speed = getSpeed(decrypt_start_time, decrypt_finish_time, TEST_DATA_SIZE);

		cout << "Encrypt: " << encrypt_speed / 1e3 << " kb/s" << endl;
		cout << "Decrypt: " << decrypt_speed / 1e3 << " kb/s" << endl;
	}
	else
		cout << "Failed" << endl;

	free(data);
	free(encrypted);
	free(decrypted);
}

unsigned char * getTestData() {
	auto data = (unsigned char *)malloc(TEST_DATA_SIZE);
	srand(time(NULL));
	fillRand(data, data + TEST_DATA_SIZE);
	return data;
}

unsigned char * getRandKey() {
	unsigned char * key = new unsigned char[KEY_LEN];
	fillRand(key, key + KEY_LEN);
	return key;
}

void fillRand(void * begin, void * end) {
	srand(time(NULL));
	for (unsigned char * x = (unsigned char *)begin; x != end; x++)
		*x = rand() % 256;
}

bool cmpArrays(void * aBegin, void * aEnd, void * bBegin, void * bEnd) {
	unsigned char * b = (unsigned char *)bBegin,
		*a = (unsigned char *)aBegin;
	for (; a != aEnd && b != bEnd; a++, b++)
	if (*a != *b)
		return false;
	return a == aEnd && b == bEnd;
}

double getSpeed(const chrono::system_clock::time_point &start, const chrono::system_clock::time_point &finish, const int data_size) {
	auto time = finish - start;
	return (double)data_size * 1e6 / chrono::duration_cast<chrono::microseconds>(time).count();
}