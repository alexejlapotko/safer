#ifndef SAFER_H
#define SAFER_H

#define R_COUNT 8
#define KEYS_COUNT (R_COUNT * 2 + 1)
#define KEY_LEN 16
#define BLOCK_LEN 16

#define MAX_BYTE 256

class SaferPlus {
public:
	SaferPlus(unsigned char * K);
	~SaferPlus();

	unsigned char ** keys() const { return K; }

	void encrypt(void * data, size_t size, void * buffer, size_t * bufferSize);
	void decrypt(void * data, size_t size, void * buffer, size_t * bufferSize);
private:
	const unsigned short byteGroup = 0x9999;


	unsigned char * key;
	unsigned char ** B;
	unsigned char ** K;
	unsigned char * nonLinearF, *invNonLinearF;
	unsigned char * bitGroup, * invBitGroup;

	void generateB();
	unsigned char * getBi(int i);
	unsigned char getBij(int i, int j);

	void generateKeys();
	unsigned char * getInitialKeysReg();
	unsigned char * getKey(unsigned char * reg, int num);
	unsigned char * selectKeyFromReg(unsigned char * reg, int num);

	void generateNonLinearF();
	void generateBitGroup();

	void encryptBlock(unsigned char *data, unsigned char * buffer);
	void decryptBlock(unsigned char *data, unsigned char * buffer);

	void makeRound(unsigned char * buffer, int r);
	void makeBackRound(unsigned char * buffer, int r);
	inline void imposeKey(unsigned char * buffer, unsigned char * key, bool invGroup, bool back = false);
	inline void nonlinearTransform(unsigned char * buffer, bool back = false);
	inline void linearTransform(unsigned char * buffer, const char matrix[][BLOCK_LEN]);

	inline void saveFpuState();
	inline void restoreFpuState();

	int powerWithMod(int x, int k, int m);
	inline int mod(int x, int m);

	static const char M[BLOCK_LEN][BLOCK_LEN];
	static const char invM[BLOCK_LEN][BLOCK_LEN];
};

#endif