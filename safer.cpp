#include "safer.h"
#include <algorithm>
#include <cmath>

SaferPlus::SaferPlus(unsigned char * key) {
	this->key = key;
	generateB();
	generateKeys();
	generateNonLinearF();
	generateBitGroup();
}

SaferPlus::~SaferPlus() {
	for (int i = 1; i < KEYS_COUNT; i++) {
		delete[] K[i];
		delete[] B[i];
	}
	delete[] K;
	delete[] B;

	delete[] nonLinearF;
	delete[] invNonLinearF;
}

void SaferPlus::encrypt(void * data, size_t size, void * buffer, size_t * bufferSize) {
	int blockCount = size / BLOCK_LEN + (bool)(size % BLOCK_LEN);
	*bufferSize = blockCount * BLOCK_LEN;

	unsigned char * bufferCursor = (unsigned char *)buffer;
	saveFpuState();
	for (int i = 0; i < blockCount; i++) {
		size_t blockBegin = i * BLOCK_LEN;
		if (blockBegin + BLOCK_LEN <= size)
			encryptBlock((unsigned char *)data + blockBegin, bufferCursor);
		else {
			unsigned char * block = new unsigned char[BLOCK_LEN];
			std::copy((unsigned char *)data + blockBegin, (unsigned char *)data + size, block);
			encryptBlock(block, bufferCursor);
			delete[] block;
		}
		bufferCursor += BLOCK_LEN;
	}
	restoreFpuState();
}

void SaferPlus::decrypt(void * data, size_t size, void * buffer, size_t * bufferSize) {
	int blockCount = size / BLOCK_LEN;
	if (size % BLOCK_LEN)
		throw "Data size must be divided by block length";
	*bufferSize = blockCount * BLOCK_LEN;

	unsigned char * bufferCursor = (unsigned char *)buffer;
	saveFpuState();
	for (unsigned char * block = (unsigned char *)data; block < (unsigned char *)data + size; block += BLOCK_LEN) {
		decryptBlock(block, bufferCursor);
		bufferCursor += BLOCK_LEN;
	}
	restoreFpuState();
}

void SaferPlus::generateB() {
	B = new unsigned char*[KEYS_COUNT];
	for (int i = 1; i < KEYS_COUNT; i++)
		B[i] = getBi(i);
}

unsigned char * SaferPlus::getBi(int i) {
	unsigned char * Bi = new unsigned char[KEY_LEN];
	for (int j = 0; j < KEY_LEN; j++)
		Bi[j] = getBij(i, j);
	return Bi;
}

unsigned char SaferPlus::getBij(int i, int j) {
	int p = powerWithMod(45, 17 * (i + 1) + (j + 1), 257);
	int f = powerWithMod(45, p, 257);
	return f == 256 ? 0 : f;
}

void SaferPlus::generateKeys() {
	K = new unsigned char *[KEYS_COUNT];
	K[0] = key;

	unsigned char * reg = getInitialKeysReg();
	for (int i = 1; i < KEYS_COUNT; i++)
		K[i] = getKey(reg, i);
}

unsigned char * SaferPlus::getInitialKeysReg() {
	unsigned char * reg = new unsigned char[KEY_LEN + 1];
	std::copy(key, key + KEY_LEN, reg);

	unsigned char sum = 0;
	for (int i = 0; i < KEY_LEN; i++)
		sum ^= reg[i];
	reg[KEY_LEN] = sum;

	return reg;
}

unsigned char * SaferPlus::getKey(unsigned char * reg, int num) {
	for (int i = 0; i < KEY_LEN + 1; i++)
		reg[i] = (reg[i] << 3) | (reg[i] >> 5);

	unsigned char * key = selectKeyFromReg(reg, num);
	for (int i = 0; i < KEY_LEN; i++)
		key[i] = ((int)B[num][i] + key[i]) % 256;

	return key;
}

unsigned char * SaferPlus::selectKeyFromReg(unsigned char * reg, int num) {
	unsigned char * key = new unsigned char[KEY_LEN];
	int pos = num;
	for (int i = 0; i < KEY_LEN; i++) {
		key[i] = reg[pos];
		pos++;
		if (pos > KEY_LEN)
			pos = 0;
	}
	return key;
}

void SaferPlus::generateNonLinearF() {
	nonLinearF = new unsigned char[MAX_BYTE];
	invNonLinearF = new unsigned char[MAX_BYTE];

	for (int i = 0; i < MAX_BYTE; i++)
		nonLinearF[i] = i == 128 ? 0 : powerWithMod(45, i, 257);
	for (int i = 0; i < MAX_BYTE; i++)
		invNonLinearF[nonLinearF[i]] = i;
}

void SaferPlus::generateBitGroup() {
	bitGroup = new unsigned char[BLOCK_LEN];
	invBitGroup = new unsigned char[BLOCK_LEN];
	for (int i = 0; i < BLOCK_LEN; i++) {
		if (byteGroup & (1 << i))
			bitGroup[i] = 0xff;
		else
			bitGroup[i] = 0;
		invBitGroup[i] = ~bitGroup[i];
	}
}

void SaferPlus::encryptBlock(unsigned char *data, unsigned char * buffer) {
	std::copy(data, data + BLOCK_LEN, buffer);
	for (int r = 0; r < R_COUNT; r++)
		makeRound(buffer, r);
	imposeKey(buffer, K[R_COUNT * 2], byteGroup);
}

void SaferPlus::decryptBlock(unsigned char *data, unsigned char * buffer) {
	std::copy(data, data + BLOCK_LEN, buffer);
	imposeKey(buffer, K[R_COUNT * 2], byteGroup, true);
	for (int r = R_COUNT - 1; r >= 0; r--)
		makeBackRound(buffer, r);
}

void SaferPlus::makeRound(unsigned char * buffer, int r) {
	imposeKey(buffer, K[r * 2], false);
	nonlinearTransform(buffer);
	imposeKey(buffer, K[r * 2 + 1], true);
	linearTransform(buffer, M);
}

void SaferPlus::makeBackRound(unsigned char * buffer, int r) {
	linearTransform(buffer, invM);
	imposeKey(buffer, K[r * 2 + 1], true, true);
	nonlinearTransform(buffer, true);
	imposeKey(buffer, K[r * 2], false, true);
}

inline void SaferPlus::imposeKey(unsigned char * buffer, unsigned char * key, bool invGroup, bool back) {
	auto bg = invGroup ? invBitGroup : bitGroup;
	auto nbg = invGroup ? bitGroup : invBitGroup;
	__asm {
		mov eax, buffer
		mov ebx, key
		mov esi, bg
		mov edi, nbg

		mov ecx, 2
		cycle:		
			movq mm0, [eax]
			pand mm0, [esi]
			movq mm1, [ebx]
			pand mm1, [esi]

			movq mm2, [eax]
			pand mm2, [edi]
			movq mm3, [ebx]
			pand mm3, [edi]

			pxor mm0, mm1

			cmp back, 0
			jne calc_sub
				paddb mm2, mm3
				jmp finish_calc
			calc_sub:
				psubb mm2, mm3
			finish_calc:

			por mm0, mm2
			movq [eax], mm0

			add eax, 8
			add ebx, 8
			add esi, 8
			add edi, 8
		loop cycle
	}
}

inline void SaferPlus::nonlinearTransform(unsigned char * buffer, bool back) {
	auto nlf = nonLinearF;
	auto inlf = invNonLinearF;
	auto bg = byteGroup;
	__asm {
		mov eax, buffer

		mov ecx, BLOCK_LEN
		mov dx, bg
		
		cmp back, 0
		je cycle
			not dx
		cycle:
			test dx, 1
			jz group2
				mov ebx, nlf
				jmp finish_iter

			group2 :
				mov ebx, inlf
			finish_iter:

			push edx
				xor edx, edx
				mov dl, [eax]
				add ebx, edx
				mov dl, [ebx]
				mov[eax], dl
			pop edx

			shr dx, 1
			inc eax
		loop cycle
	}
}

inline void SaferPlus::linearTransform(unsigned char * buffer, const char matrix[][BLOCK_LEN]) {
	unsigned char res[BLOCK_LEN];
	__asm {
		mov esi, buffer
		mov edi, matrix
		lea edx, res
		mov ecx, BLOCK_LEN
		cycle1:
			xor bh, bh	

			mov al, [esi]
			mul byte ptr[edi]
			add bh, al

			mov al, [esi + 1]
			mul byte ptr [edi + 1]
			add bh, al

			mov al, [esi + 2]
			mul byte ptr[edi + 2]
			add bh, al

			mov al, [esi + 3]
			mul byte ptr[edi + 3]
			add bh, al

			mov al, [esi + 4]
			mul byte ptr[edi + 4]
			add bh, al

			mov al, [esi + 5]
			mul byte ptr[edi + 5]
			add bh, al

			mov al, [esi + 6]
			mul byte ptr[edi + 6]
			add bh, al

			mov al, [esi + 7]
			mul byte ptr[edi + 7]
			add bh, al

			mov al, [esi + 8]
			mul byte ptr[edi + 8]
			add bh, al

			mov al, [esi + 9]
			mul byte ptr[edi + 9]
			add bh, al

			mov al, [esi + 10]
			mul byte ptr[edi + 10]
			add bh, al

			mov al, [esi + 11]
			mul byte ptr[edi + 11]
			add bh, al

			mov al, [esi + 12]
			mul byte ptr[edi + 12]
			add bh, al

			mov al, [esi + 13]
			mul byte ptr[edi + 13]
			add bh, al

			mov al, [esi + 14]
			mul byte ptr[edi + 14]
			add bh, al

			mov al, [esi + 15]
			mul byte ptr[edi + 15]
			add bh, al

			jmp skip
			cycle2 :
				jmp cycle1
			skip :
			
			mov[edx], bh

			add edi, BLOCK_LEN
			inc edx
		loop cycle2
	}
	std::copy(res, res + BLOCK_LEN, buffer);
}

char fpu_buff[108];

inline void SaferPlus::saveFpuState() {
	__asm {
		lea esi, fpu_buff
		pushf
		fsave[esi]
		fwait
		popf
	}
}

inline void SaferPlus::restoreFpuState() {
	__asm {
		lea esi, fpu_buff
		pushf
		frstor[esi]
		fwait
		popf
	}
}

int SaferPlus::powerWithMod(int x, int k, int m) {
	int res = 1;
	while (k) {
		if (k & 1)
			res = (res * x) % m;
		x = (x * x) % m;
		k >>= 1;
	}
	return res;
}

inline int SaferPlus::mod(int x, int m) {
	while (x < 0)
		x += m;
	return x % m;
}

const char SaferPlus::M[BLOCK_LEN][BLOCK_LEN] =
	{ { 2, 2, 1, 1, 16, 8, 2, 1, 4, 2, 4, 2, 1, 1, 4, 4 },
	{ 1, 1, 1, 1, 8, 4, 2, 1, 2, 1, 4, 2, 1, 1, 2, 2 },
	{ 1, 1, 4, 4, 2, 1, 4, 2, 4, 2, 16, 8, 2, 2, 1, 1 },
	{ 1, 1, 2, 2, 2, 1, 2, 1, 4, 2, 8, 4, 1, 1, 1, 1 },
	{ 4, 4, 2, 1, 4, 2, 4, 2, 16, 8, 1, 1, 1, 1, 2, 2 },
	{ 2, 2, 2, 1, 2, 1, 4, 2, 8, 4, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 4, 2, 4, 2, 16, 8, 2, 1, 2, 2, 4, 4, 1, 1 },
	{ 1, 1, 2, 1, 4, 2, 8, 4, 2, 1, 1, 1, 2, 2, 1, 1 },
	{ 2, 1, 16, 8, 1, 1, 2, 2, 1, 1, 4, 4, 4, 2, 4, 2 },
	{ 2, 1, 8, 4, 1, 1, 1, 1, 1, 1, 2, 2, 4, 2, 2, 1 },
	{ 4, 2, 4, 2, 4, 4, 1, 1, 2, 2, 1, 1, 16, 8, 2, 1 },
	{ 2, 1, 4, 2, 2, 2, 1, 1, 1, 1, 1, 1, 8, 4, 2, 1 },
	{ 4, 2, 2, 2, 1, 1, 4, 4, 1, 1, 4, 2, 2, 1, 16, 8 },
	{ 4, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 2, 1, 8, 4 },
	{ 16, 8, 1, 1, 2, 2, 1, 1, 4, 4, 2, 1, 4, 2, 4, 2 },
	{ 8, 4, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 2, 1, 4, 2 } };

const char SaferPlus::invM[BLOCK_LEN][BLOCK_LEN] =
	{ { 2, -2, 1, -2, 1, -1, 4, -8, 2, -4, 1, -1, 1, -2, 1, -1 },
	{ -4, 4, -2, 4, -2, 2, -8, 16, -2, 4, -1, 1, -1, 2, -1, 1 },
	{ 1, -2, 1, -1, 2, -4, 1, -1, 1, -1, 1, -2, 2, -2, 4, -8 },
	{ -2, 4, -2, 2, -2, 4, -1, 1, -1, 1, -1, 2, -4, 4, -8, 16 },
	{ 1, -1, 2, -4, 1, -1, 1, -2, 1, -2, 1, -1, 4, -8, 2, -2 },
	{ -1, 1, -2, 4, -1, 1, -1, 2, -2, 4, -2, 2, -8, 16, -4, 4 },
	{ 2, -4, 1, -1, 1, -2, 1, -1, 2, -2, 4, -8, 1, -1, 1, -2 },
	{ -2, 4, -1, 1, -1, 2, -1, 1, -4, 4, -8, 16, -2, 2, -2, 4 },
	{ 1, -1, 1, -2, 1, -1, 2, -4, 4, -8, 2, -2, 1, -2, 1, -1 },
	{ -1, 1, -1, 2, -1, 1, -2, 4, -8, 16, -4, 4, -2, 4, -2, 2 },
	{ 1, -2, 1, -1, 4, -8, 2, -2, 1, -1, 1, -2, 1, -1, 2, -4 },
	{ -1, 2, -1, 1, -8, 16, -4, 4, -2, 2, -2, 4, -1, 1, -2, 4 },
	{ 4, -8, 2, -2, 1, -2, 1, -1, 1, -2, 1, -1, 2, -4, 1, -1 },
	{ -8, 16, -4, 4, -2, 4, -2, 2, -1, 2, -1, 1, -2, 4, -1, 1 },
	{ 1, -1, 4, -8, 2, -2, 1, -2, 1, -1, 2, -4, 1, -1, 1, -2 },
	{ -2, 2, -8, 16, -4, 4, -2, 4, -1, 1, -2, 4, -1, 1, -1, 2 } };