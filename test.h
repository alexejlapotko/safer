#ifndef TEST_H
#define TEST_H

#ifdef _DEBUG
#define TEST_DATA_SIZE 100000
#else
#define TEST_DATA_SIZE 1000000
#endif

void test();

#endif